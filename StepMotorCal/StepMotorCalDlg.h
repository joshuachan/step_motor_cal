// StepMotorCalDlg.h : 头文件
//

#pragma once
#include "afxwin.h"
#include "ModCal.h"

// CStepMotorCalDlg 对话框
class CStepMotorCalDlg : public CDialog
{
// 构造
public:
	CStepMotorCalDlg(CWnd* pParent = NULL);	// 标准构造函数

// 对话框数据
	enum { IDD = IDD_STEP_MOTOR_CAL_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持


// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

public:
	afx_msg void OnBnClickedButtonClearArgs();
public:
	afx_msg void OnBnClickedButtonSetDefault();
public:
	afx_msg void OnBnClickedRadioBaseOnSmSpeed();
public:
	afx_msg void OnBnClickedRadioBaseOnPulseFreq();
public:
	afx_msg void OnBnClickedButtonCurveCal();
public:
	afx_msg void OnBnClickedButtonClearTable();
public:
	afx_msg void OnBnClickedButton1();
public:
	afx_msg void OnBnClickedButtonReadme();
};
